package cs.mad.flashcards.entities

data class Flashcard(val term: String, val definition: String){
    fun genFlashcards(): MutableList<Flashcard> {
        return mutableListOf(
            Flashcard("H", "Hydrogen"),
            Flashcard("He", "Helium"),
            Flashcard("Li", "Lithium"),
            Flashcard("Be", "Beryllium"),
            Flashcard("B", "Boron"),
            Flashcard("C", "Carbon"),
            Flashcard("N", "Nitrogen"),
            Flashcard("O", "Oxygen"),
            Flashcard("F", "Fluorine"),
            Flashcard("Ne", "Neon")
        )
    }

    fun flashcardToString(): String {
        return this.term + ", " + this.definition
    }
}
