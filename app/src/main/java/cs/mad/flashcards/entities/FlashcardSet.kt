package cs.mad.flashcards.entities

data class FlashcardSet(val title: String){
    fun genFlashcardSets(): MutableList<FlashcardSet> {
        return mutableListOf(
            FlashcardSet("1"),
            FlashcardSet("2"),
            FlashcardSet("3"),
            FlashcardSet("4"),
            FlashcardSet("5"),
            FlashcardSet("6"),
            FlashcardSet("7"),
            FlashcardSet("8"),
            FlashcardSet("9"),
            FlashcardSet("10")
        )
    }
    fun flashcardSetToString(): String {
        return this.title
    }
}
