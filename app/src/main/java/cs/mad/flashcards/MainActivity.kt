package cs.mad.flashcards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //flashcard test
        Log.d("TEST", "Flashcard Test")
        val c = Flashcard("test", "testing")
        val cards = c.genFlashcards()
        for (card in cards) {
            Log.d("TEST", card.flashcardToString())
        }
        //flashcardset test
        Log.d("TEST", "FlashcardSet Test")
        val s = FlashcardSet("test")
        val sets = s.genFlashcardSets()
        for (set in sets) {
            Log.d("TEST", set.flashcardSetToString())
        }
    }
}